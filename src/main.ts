import * as application from "./application/index.js";
import "./components/main.js";

declare global {
    const applicationContext: application.Context;
    const applicationSettings: application.Settings;

    interface Window {
        applicationContext: application.Context
        applicationSettings: application.Settings
    }
}

window.applicationContext = application.createContext(
    window.applicationSettings,
);
