import { serializeJs } from "../utils/serialize-js.js";
import * as application from "./index.js";

export function renderMainHtml(applicationSettings: application.Settings) {
    return `<!DOCTYPE html>
    
<html lang="en">

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="robots" content="noindex,nofollow">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="theme-color" content="#000000"/>

<style>

:root {
    --font-size: 15px;
    --padding: 10px;
    --background-color: white;
    --dark-color: black;
}

body {
    all: initial;
    background-color: var(--background-color);
}
  
</style>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Latency.GG</title>

<script>
window.applicationSettings = ${serializeJs(applicationSettings)};
</script>

<script src="/main.js"></script>
</head>

<body>
    <app-root></app-root>
</body>

</html>
`;
}
