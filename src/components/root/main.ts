import * as index from "./index.js";

declare global {
    interface HTMLElementTagNameMap {
        "app-root": index.RootComponent;
    }
}

customElements.define("app-root", index.RootComponent);

