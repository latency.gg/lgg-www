import { html } from "lit";
import { ComponentBase } from "../base.js";

export class HomeRouteComponent extends ComponentBase {
    render() {
        return html`
<app-logo></app-logo>

<p>
    We're a not-for-profit organisation giving game developers more accurate latency measurements. We’re
    independent, open-source and hosted in the same data centres as the games themselves. Because that’s good
    for everybody.
</p>

<h2>
    Good measurements make good games
</h2>
<p>
    We integrate directly with your matchmaker, so you can use your player’s latency to give them the fastest
    connection possible, cut down the jitter in game and make more fairly balanced matches.
</p>

<h2>
    Create a better experience
</h2>
<p>
    When you use us, you can ask for aggregated data and statistics about your performance. So you can improve
    your connectivity, without spending a dime.
</p>

<h2>
    Why open-source and not-for-profit?
</h2>
<p>
    We think the only way to completely trust the data is accurate and fair is to be completely open. It means
    we can work with everyone – ISPs, developers and hosting providers – while staying unbiased.
</p>
<p>
    So we want everyone to know how we measure, what we measure and why we measure. As well as all the
    mathematics behind our analysis.
</p>
`;
    }
}

