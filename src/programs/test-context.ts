import { program } from "commander";
import { withContext } from "../testing/index.js";

program.
    command("test-context").
    action(action);

async function action() {
    console.log("starting...");

    await withContext(async context => {
        console.log("started");

        await new Promise(resolve => process.addListener("SIGINT", resolve));

        console.log("stopping...");
    });

    console.log("stopped");
}
